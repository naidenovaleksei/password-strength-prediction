import re
import json
import logging
import os

from flask import Flask
from flask import request, render_template

from model import NBModel

logging.basicConfig(
    format='%(levelname)s:%(message)s',
    level=logging.INFO
)


app = Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True

DEFAULT_PASSWORD = "password"

DIRNAME = os.path.dirname(os.path.abspath(__file__))
app.model = NBModel(f"{DIRNAME}/models/model.bin")


@app.route("/", methods=["GET"])
def index_form():
    return render_template("index.html", password=DEFAULT_PASSWORD)


@app.route("/", methods=["POST"])
def index():
    password = request.form['password'] or DEFAULT_PASSWORD
    prediction = app.model.predict(password)
    return render_template("index.html", password=password, prediction=prediction)


if __name__ == "__main__":
    app.run()
