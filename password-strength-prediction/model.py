import re
import pickle
import logging

from sklearn.naive_bayes import BernoulliNB

PATTERN_MATCHES = [
    r'^[\d]{4}$', r'^[\d]{6}$', r'^[\d]{8}$', r'^[\d]{8,}$', r'^[\d]{4,8}$',
    r'^[\d]+$', r'^[a-z]+$', r'^[A-Z]', r'^[A-Z]+$', r'^[A-Z][a-z]',
    r'^[A-Za-z]+[0-9]+$', r'^[A-Za-z]+1$', r'^1[A-Za-z]+$', r'^[A-Z]+[0-9]+$', r'^[a-z]+[0-9]+$',
    r'^[0-9]+[a-z]+$', r'^[0-9]+[a-zA-Z]+$', r'^[a-z]+[0-9]{3,}$', r'^[a-z]+[0-9]$', r'^[a-z]+[0-9]{1,3}$', r'^[A-Za-z]+[0-9]+[A-Za-z]+', r'^[A-Za-z]+[0-9]+[A-Za-z]+[0-9]+',
    r'([\d]{2,})(\1)+', r'.*([\w]+)(\1)+.*'
    
]

class NBModel:
    def __init__(self, fname, pattern_matches=PATTERN_MATCHES):
        with open(fname, "rb") as f:
            data = pickle.load(f)
        self.consts = {
            0: data["c2"],
            1: data["c1"]
        }
        self.model = data["model"]
        assert isinstance(self.model, BernoulliNB), "model must be BernoulliNB"
        self.pattern_matches = pattern_matches
    
    def predict(self, input_data):
        data = [
            int(re.match(pat, input_data) is not None)
            for pat in self.pattern_matches
        ]
        pred_class = int(self.model.predict([data])[0])
        logging.info(f"input_data = '{input_data}; pred_class = {pred_class}; data = {data}")
        return self.consts[pred_class]
