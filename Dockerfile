# https://hub.docker.com/_/python
FROM python:3.7-slim

WORKDIR /app
COPY password-strength-prediction/*.py ./
COPY password-strength-prediction/models ./models
COPY password-strength-prediction/templates ./templates
COPY deploy/* ./
RUN pip install pipenv
RUN pipenv install --deploy --system

CMD /app/entypoint.sh