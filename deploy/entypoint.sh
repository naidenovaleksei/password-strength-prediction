#!/bin/sh
pipenv run gunicorn --reload \
    --workers=1 --bind=0.0.0.0:$PORT app:app