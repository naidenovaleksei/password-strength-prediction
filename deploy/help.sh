# build docker container
docker build -t password-strength-prediction:1.0.0 ..
docker run -it --rm -p 8080:8080 -e PORT=8080 password-strength-prediction:1.0.0 
password-strength-prediction:1.0.0

# deploy to heroku
heroku container:login
heroku create password-na
docker tag password-strength-prediction:1.0.0 registry.heroku.com/password-na/web
docker push registry.heroku.com/password-na/web
heroku container:release web --app password-na

# open deploy in heroku
heroku open --app password-na